document.body.innerHTML = '<div id="story"></div><button id="next">næsta borg</button>'; 

var { createStory, renderStory } = require('../main.js');

test('The should always be a <h1> tag inside the story', function() {
    expect(createStory() ).toMatch(/<h1>/);
});

test('there should be an <img> tag inside the story', function() {
    expect(createStory() ).toMatch(/<img/);
});

test('there should be at least one <p> tag inside the story', function () {
    expect(createStory() ).toMatch(/<p>/);
});


test('there should be a <h1> element inside the main div', function() {
   var getFromDom = function() {
       renderStory();
       return document.querySelector("#story").innerHTML;
   } 
   console.log(getFromDom()); 
    expect(getFromDom()).toMatch(/<h1>/); 
});